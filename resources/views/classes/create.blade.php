@extends('main')
@section('content')
<div class="row">
  <div class="col-md-6 offset-md-3">
    <div class="card">
      <div class="card-header">
        Create a class
      </div>
      <div class="card-body">
      <form method="POST" action="{{route('classes.store')}}">
        @csrf  
        <div class="form-group">
            <label for="class">Class Title</label>
            <input type="text" class="form-control" id="class" name="class">
            @error('class')
               <span class="text-danger">{{$message}}</span> 
            @enderror
          </div>
          <div class="form-group">
            <label for="time">Date and Time</label>
            <input type="datetime-local" class="form-control" id="time" name="time">
            @error('time')
            <span class="text-danger">{{$message}}</span> 
         @enderror
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection