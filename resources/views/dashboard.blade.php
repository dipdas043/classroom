@extends('main')
@section('content')
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
              @if(count($classes) > 0)
              @foreach ($classes as $classes)
              <div class="jumbotron jumbotron-fluid"> 
                <div class="container">
                  <h1 class="display-4"><a href="/classes/{{$classes->id}}">{{$classes->class}}</a></h1>
                  <p class="lead"><strong>{{$classes ->time}}</strong></p>
                </div>
              </div> 
              @endforeach
              @else
              <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('No Class created yet') }}
            </h2> 
            @endif 
            </div>
        </div>
    </div>
</x-app-layout>
@endsection