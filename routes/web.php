<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\ClassController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Classes;
use Framework\Sessions;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/', [PagesController::class, 'index'])->name('/');
Route::resource('classes', ClassController::class);
Route::get('/create', [ClassController::class, 'create']);
//Route::any('/createclass', [ClassController::class, 'show'])->name('createclass.show');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    $classes = Classes::orderBy('class','desc')->get();
    return view('dashboard')->with('classes',$classes);
    //return view('dashboard');
})->name('dashboard');
